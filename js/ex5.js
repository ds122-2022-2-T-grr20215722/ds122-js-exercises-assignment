
let s = 'Aula de web1'

function contaLetras(s, letra) {
  let resultado = 0

  for (let i = 0; i < s.length; i++) {
    if (s[i] == letra) {
      resultado++
    }
  }

  return resultado
}

console.log(contaLetras(s, 'e'))
