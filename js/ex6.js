let s = 'Aula de web1'

function procuraSubStr(str, sub) {
  
  for (let i = 0; i < str.length - sub.length + 1; i++) {
    if (str[i] !== sub[0]) continue
    let exists = true
    for (let j = 1; j < sub.length && exists; j++) {
      if (str[i + j] === sub[j]) continue
      exists = false
    }
    if (exists == true) 
    return i
  }
  return false
}


console.log(procuraSubStr(s, 'web'))
